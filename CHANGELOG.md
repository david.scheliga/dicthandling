# Changelog
This changelog is inspired by [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [0.2b2] - 2020-10-10
### Fixed
- Bug in *overlap_branches* leading to exception.

### Added
- doctest to *overlap_branches*

### Changed
- print_tree removes trailing whitespaces.

## [0.2b0] - 2020-09-21
### Added
- Method *try_decoding_potential_json_content* tries to decode a byte-like object to
  a string, by trying a set of encodings by default.

## [0.1b8] - 2020-07-07
### Fixed
- *max_itemlength* and *hide_leading_underscores* are now passed to deeper levels.
  
## [0.1b7] - 2020-07-02
### Added
- *hide_empty* to print_tree to hide dictionary fields with empty strings or
  None values.

## [0.1b6] - 2020-07-02
### Fixed
- print_tree indents is now properly.

### Changed
- Argument *_parents_indent* changed to *starting_indent* and added to the docstring,
  as it allows to define a starting intent.

### Added
- redirection __init__
- release.sh bash script

### Fixed
- removed svg-graphic from link, which made pycharm crashing

## [0.1b5] - 2020-05-26
### Fixed
- Fixed bug erasing complete json file content, if provided data contained an item,
  which could not be encoded by the json module.
- put_into_json_file added double entries if dictionary for cases in which dictionary
  keys were numeric values.
- Rare occurrence of UTF-8 de-&encoding errors if files are interchanged in between 
  Windows and Linux
- Wrong docstring of `read_from_json_file`
- Wrong formatting of `put_into_json_file`
- Linked image markdown line for 'code-black' lead pycharm to crash; 
  replaced linked image with standard link 

### Added
- FutureWarning for `flatten` and `unflatten` to be renamed in future
  releases to `flatten_for_config` and `unflatten_from_config`.
- Added docstring to `deep_unflatten`

### Deprecated
- `deep_flatten` and `deep_unflatten` will be replaced by `flatten_dict`
   and `unflatten_dict`.

### Changed
- Logger changed from exa.dicthandling to dicthandling.

## [0.1b4] - 2020-01-17
### Added
- Changelog document is added to this project.

### Fixed
- Incorrect identifiers within setup.py
- Wrong link to documentation within README

### Changed
- Code maintenance within `dicthandling.py`
- Reworked README 

## [0.1b1] - 2020-01-16
### Added
- Outsourcing and releasing `dicthandling` on https://gitlab.com
