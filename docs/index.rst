.. dicthandling documentation master file, created by
   sphinx-quickstart on Thu Jan 16 13:55:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation of the module dicthandling
========================================
.. py:currentmodule:: dicthandling

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Dicthandling contains convenient methods to work with nested dictionaries.

Using :func:`print_tree` returns a pretty print of a nested dictionary, which
you may find more readable than the standard output.

Further functions of this module are

- for accessing data

   - :func:`get_leaf` gets an item at an address
   - :func:`set_leaf` sets an item at an address
   - :func:`update_only_values`
   - :func:`add_missing_branches`
   - :func:`overlap_branches`
   - :func:`join_address`

- for creating nested dictionaries

   - :func:`get_unused_key` gets a key which is not in the existing dictionary
   - :func:`unfold_branch` creates a nested dictionary on base of an address

- Configparser related functions

   - :func:`flatten`
   - :func:`deep_flatten`
   - :func:`unflatten`
   - :func:`deep_unflatten`

- for storing and reading with the json module

   - :func:`put_into_json_file`
   - :func:`read_from_json_file`

General
-------

.. autofunction:: print_tree
.. autofunction:: keep_keys

Accessing
---------

.. autofunction:: get_leaf
.. autofunction:: set_leaf
.. autofunction:: update_only_values
.. autofunction:: add_missing_branches
.. autofunction:: overlap_branches

.. attribute:: ADDRESS_DELIMITER

   Delimiter by which parts of a path within a dictionary are separated.

.. autofunction:: join_address

Creation
--------

.. autofunction:: get_unused_key
.. autofunction:: unfold_branch



Configparser related functions
------------------------------

.. autofunction:: flatten

 Example:
     A nested dicionary like ..
     ::

         root
         |- comment : "toplevel item"
         |- project : { }
                       |- editor : Fry
                       |- year : 4029
                       |- custumor : Planet Express
                       |- paths : { }
                                   |- rawdatapath : "/tmp/here"
                                   |- processedpath : "/tmp/there"

     will be converted into ..
     ::

         root
         |- DEFAULT : { }
         |             |- comment : "toplevel item"
         |
         |- project : { }
         |             |- editor : Fry
         |             |- year : 4029
         |             |- custumor : Planet Express
         |
         |- project/paths : { }
                             |- rawdatapath : "/tmp/here"
                             |- processedpath : "/tmp/there"


.. autofunction:: deep_flatten

.. autofunction:: unflatten
.. autofunction:: deep_unflatten


Storing and reading with the json module
----------------------------------------

.. autofunction:: try_decoding_potential_json_content
.. autofunction:: put_into_json_file
.. autofunction:: read_from_json_file
